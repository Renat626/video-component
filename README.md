# Video-component



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/Renat626/video-component.git
git branch -M main
git push -uf origin main
```

## Работа компонента

Чтобы компонент отобразил видео, нужно зайти на нужное видео в youtube и после нажатия правой кнопки мыши по видео, нажать на кнопку "Копировать HTML-код". 
Скопируется вот такой код: iframe width="1280" height="720" src="https://www.youtube.com/embed/ywPGwmM6tiU" title="Что могут хакеры? В школе такому точно не научат. Профессионалы.." frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen>/iframe>. Ссылку нужно достать из атрибута src.