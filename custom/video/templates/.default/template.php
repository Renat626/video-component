<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
    //var_dump($arResult);

    $vertical = "";
    $gorizontal = "";

    if ($arResult["VERTICAL"] == "TOP") {
        $vertical = "videoComponent_top";
    } else if ($arResult["VERTICAL"] == "BOTTOM") {
        $vertical = "videoComponent_bottom";
    } else {
        $vertical = "videoComponent_top";
    }

    if ($arResult["GORIZONTAL"] == "LEFT") {
        $gorizontal = "videoComponent_left";
    } else if ($arResult["GORIZONTAL"] == "RIGHT") {
        $gorizontal = "videoComponent_right";
    } else {
        $gorizontal = "videoComponent_left";
    }
?>
    <?if ($arResult["LINK_VIDEO"] != NULL) :?>
    <div class="videoComponent <?=$vertical?> <?=$gorizontal?>">
        <iframe width="100%" height="100%" src="<?=$arResult["LINK_VIDEO"]?>" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>
    <?endif;?>
<?php
?>