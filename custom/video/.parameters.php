<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
$arComponentParameters = array(
	"PARAMETERS" => array(
        "LINK_VIDEO" => array(
            "PARENT" => "BASE",
            "NAME" => "Ссылка на видео",
            "TYPE" => "STRING",
            "DEFAULT" => NULL
        ),
        "VERTICAL" => array(
            "PARENT" => "BASE",
            "NAME" => "Вертикальное расположение",
            "TYPE" => "LIST",
            "VALUES" => array(
                "TOP" => "Вверху",
                "BOTTOM" => "Внизу"
            ),
            "DEFAULT" => "TOP"
        ),
        "GORIZONTAL" => array(
            "PARENT" => "BASE",
            "NAME" => "Гаризонтальное расположение",
            "TYPE" => "LIST",
            "VALUES" => array(
                "LEFT" => "Слева",
                "RIGHT" => "Справа"
            ),
            "DEFAULT" => "LEFT"
        )
    )
);
?>